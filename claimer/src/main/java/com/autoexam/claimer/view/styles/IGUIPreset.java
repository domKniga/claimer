package com.autoexam.claimer.view.styles;

import java.awt.Component;

/**
 * Functional interface representing a GUI preset responsible for styling common
 * UI components.
 * 
 * @author Georgi Iliev
 *
 */
@FunctionalInterface
public interface IGUIPreset {

    /**
     * Styles the specified component in the appropriate manner.
     * 
     * @param component
     *            - the component to be styled.
     */
    public void style(final Component component);

}
