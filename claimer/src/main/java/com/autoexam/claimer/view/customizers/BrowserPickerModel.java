package com.autoexam.claimer.view.customizers;

import javax.swing.DefaultComboBoxModel;

import com.autoexam.claimer.model.enums.SupportedBrowsers;

/**
 * Custom combo box model for browser picker combo box. Intended use in this
 * case is to restrict selection of currently not recommended browsers.
 * 
 * @author Georgi Iliev
 */
public class BrowserPickerModel extends DefaultComboBoxModel<SupportedBrowsers> {

    private static final long serialVersionUID = 3188196843396975309L;

    public BrowserPickerModel() {
    }

    public BrowserPickerModel(final SupportedBrowsers[] items) {
	super(items);
    }

    @Override
    public void setSelectedItem(Object item) {
	if (!(((SupportedBrowsers) item).isSelectable())) {
	    return;
	}

	super.setSelectedItem(item);
    };

}
