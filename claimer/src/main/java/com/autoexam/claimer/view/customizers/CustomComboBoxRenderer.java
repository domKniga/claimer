package com.autoexam.claimer.view.customizers;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 * Custom combo box renderer.
 * 
 * @author Georgi Iliev.
 */
public class CustomComboBoxRenderer implements ListCellRenderer<Object> {

    protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();

    private Font font;
    private Color foreground;
    private Color background;
    private Color selectionForeground;
    private Color selectionBackground;

    public CustomComboBoxRenderer(Font font, Color foreground, Color background, Color selectionForeground,
	    Color selectionBackground) {
	this.font = font;
	this.foreground = foreground;
	this.background = background;
	this.selectionForeground = selectionForeground;
	this.selectionBackground = selectionBackground;
    }

    @Override
    public Component getListCellRendererComponent(JList<? extends Object> list, Object value, int index,
	    boolean isSelected, boolean cellHasFocus) {
	
	list.setForeground(foreground);
	list.setBackground(background);

	list.setSelectionForeground(selectionForeground);
	list.setSelectionBackground(selectionBackground);

	list.setFont(font);

	return (JLabel) defaultRenderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
    }

}
