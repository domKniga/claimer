package com.autoexam.claimer.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.autoexam.claimer.model.enums.Folders;
import com.autoexam.claimer.model.enums.SupportedBrowsers;

/**
 * The claimer model.
 * 
 * @author Georgi Iliev
 */
public class ClaimerModel {

    private static final Logger LOGGER = Logger.getLogger(ClaimerModel.class);

    public static final String KEY_LICENSE_NUMBER = "licenseNumber";
    public static final String KEY_STARTING_URL = "startingUrl";

    public static final String DEFAULT_STARTING_URL = "https://avtoizpit.com/motor-exam-result-web/";
    public static final String DEFAULT_LICENSE_NUMBER = "N/A";

    private String settingsFileName;
    private Properties settingsFile;

    private SupportedBrowsers[] browsers;
    private String startingUrl;
    private String licenseNumber;

    /**
     * Creates a model with default state.
     */
    public ClaimerModel() {
	this.browsers = SupportedBrowsers.values();
	this.startingUrl = DEFAULT_STARTING_URL;
	this.licenseNumber = DEFAULT_LICENSE_NUMBER;
    }

    /**
     * Builds a model from configuration.
     * 
     * @param settingsFileName
     *            - the name of the configuration settings file.
     * @throws IOException
     */
    public ClaimerModel(final String settingsFileName) throws IOException {
	this.settingsFile = new Properties();
	this.settingsFileName = settingsFileName;

	refreshSettings();

	this.browsers = SupportedBrowsers.values();
	this.startingUrl = this.settingsFile.getProperty(KEY_STARTING_URL, DEFAULT_STARTING_URL);
	this.licenseNumber = this.settingsFile.getProperty(KEY_LICENSE_NUMBER, DEFAULT_LICENSE_NUMBER);
    }

    /**
     * Stores the settings input in the view, so that they are available on next
     * startup.
     * 
     * @param currentSettings
     *            - a map of currently input settings(in the view).
     * @throws IOException
     *             If there is a problem accessing the property file.
     */
    public void saveSettings(final Map<String, String> currentSettings) throws IOException {
	boolean settingsChanged = currentSettings.entrySet().stream() // nl
		.anyMatch(entry -> !(settingsFile.contains(entry.getValue())));

	if (settingsChanged) {
	    File outputFile = new File(
		    Paths.get(Folders.HOME_FOLDER.identifier(), Folders.CONFIG_FOLDER.identifier(), settingsFileName)
			    .toString());
	    try (OutputStream outputFileAsStream = new FileOutputStream(outputFile);) {
		LOGGER.info("BEFORE: " + settingsFile);

		settingsFile.putAll(currentSettings);
		settingsFile.store(outputFileAsStream, "");

		LOGGER.info("AFTER: " + settingsFile);
	    } finally {
		LOGGER.warn("Settings modified and stored!");
		LOGGER.info("----------------------------");
	    }
	}
    }

    /**
     * @throws IOException
     *             If there is a problem accessing the property file.
     */
    public void refreshSettings() throws IOException {
	try (InputStream settingsAsStream = new FileInputStream(new File(
		Paths.get(Folders.HOME_FOLDER.identifier(), Folders.CONFIG_FOLDER.identifier(), settingsFileName)
			.toString()));) {
	    settingsFile.load(settingsAsStream);
	} finally {
	    LOGGER.info("Read settings:");
	    LOGGER.info("----------------------------");
	    LOGGER.info(settingsFile);
	}
    }

    public SupportedBrowsers[] getBrowsers() {
	return this.browsers;
    }

    public String getStartingUrl() {
	return this.startingUrl;
    }

    public String getLicenseNumber() {
	return this.licenseNumber;
    }
}
