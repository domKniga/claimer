package com.autoexam.claimer.controller;

import java.awt.event.ActionEvent;

/**
 * Functional interface for "RUN" operation - handles starting the chosen
 * browser with the specified URL.
 * 
 * @author Georgi Iliev
 */
@FunctionalInterface
public interface IRunBrowser {

    /**
     * Starts the chosen browser with the specified URL.
     * 
     * @param startBrowsing
     *            - the button click event.
     */
    void runBrowser(ActionEvent startBrowsing);

}
