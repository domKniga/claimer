package com.autoexam.claimer.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.LookAndFeel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.FontUIResource;
import javax.swing.plaf.nimbus.NimbusLookAndFeel;

import org.apache.log4j.Logger;

import com.autoexam.claimer.model.enums.SupportedBrowsers;
import com.autoexam.claimer.view.styles.AbstractGUIPreset;

/**
 * The claimer view(UI).
 * 
 * @author Georgi Iliev
 */
public class ClaimerView extends JFrame {

    private static final long serialVersionUID = -3031359406761599916L;

    private static final Logger LOGGER = Logger.getLogger(ClaimerView.class);

    private AbstractGUIPreset preset;

    private JComboBox<SupportedBrowsers> browserPicker;
    private JTextField urlField;
    private JTextField licenseField;
    private JButton startButton;

    public ClaimerView(AbstractGUIPreset preset) {
	super("Автобот");
	this.preset = preset;

	setLookAndFeel();
	this.preset.style(this);

	addSettingsPanel();
	addConfirmPanel();
    }

    /**
     * Binds the default button, window optimal size and position and displays
     * the frame.
     */
    public void display() {
	SwingUtilities.getRootPane(startButton).setDefaultButton(startButton);

	this.pack();
	this.setLocationRelativeTo(null);
	this.setVisible(true);
    }

    private void setLookAndFeel() {
	UIManager.LookAndFeelInfo iLAFs[] = UIManager.getInstalledLookAndFeels();

	for (int i = 0; i < iLAFs.length; i++) {
	    if (iLAFs[i].getName().equals("Nimbus")) {

		LookAndFeel nimbus = new NimbusLookAndFeel();
		nimbus.getDefaults().put("ToolTip.font", new FontUIResource("Rockwell", Font.BOLD, 16));

		try {
		    UIManager.setLookAndFeel(nimbus);
		} catch (UnsupportedLookAndFeelException e) {
		    LOGGER.error("Problem setting look & feel! Defaulting to Metal LAF...", e);
		    break;
		}

		return;
	    }
	}
    }

    private void addSettingsPanel() {
	final JLabel browserLabel = new JLabel("Браузър:");
	preset.style(browserLabel);

	browserPicker = new JComboBox<SupportedBrowsers>();
	preset.style(browserPicker);

	final JPanel browserPane = new JPanel(new FlowLayout(FlowLayout.CENTER));
	browserPane.setOpaque(false);
	browserPane.add(browserLabel);
	browserPane.add(browserPicker);

	final JLabel urlLabel = new JLabel("Адрес(линк):");
	preset.style(urlLabel);

	urlField = new JTextField();
	preset.style(urlField);

	final JPanel urlPane = new JPanel(new FlowLayout(FlowLayout.CENTER));
	urlPane.setOpaque(false);
	urlPane.add(urlLabel);
	urlPane.add(urlField);

	final JLabel licenseNumberLabel = new JLabel("№ на разрешително:");
	preset.style(licenseNumberLabel);

	licenseField = new JTextField();
	preset.style(licenseField);
	// TODO: [TEMP]
	licenseField.setEditable(false);

	final JPanel licensePane = new JPanel(new FlowLayout(FlowLayout.CENTER));
	licensePane.setOpaque(false);
	licensePane.add(licenseNumberLabel);
	licensePane.add(licenseField);

	final Box settingsBox = Box.createVerticalBox();
	settingsBox.setOpaque(false);
	settingsBox.add(browserPane);
	settingsBox.add(urlPane);
	settingsBox.add(licensePane);

	final JScrollPane settingsScroll = new JScrollPane(settingsBox);
	preset.style(settingsScroll);

	this.getContentPane().add(settingsScroll, BorderLayout.NORTH);
    }

    private void addConfirmPanel() {
	startButton = new JButton("СТАРТ");
	preset.style(startButton);

	final JPanel confirmPane = new JPanel(new FlowLayout(FlowLayout.CENTER));
	confirmPane.setOpaque(false);
	confirmPane.add(startButton);

	this.getContentPane().add(confirmPane, BorderLayout.SOUTH);
    }

    public JTextField getURLField() {
	return this.urlField;
    }

    public JTextField getLicenseField() {
	return this.licenseField;
    }

    public JComboBox<SupportedBrowsers> getBrowserPicker() {
	return this.browserPicker;
    }

    public JButton getStartButton() {
	return this.startButton;
    }
}
