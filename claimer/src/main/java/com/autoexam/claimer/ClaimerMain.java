package com.autoexam.claimer;

import java.io.IOException;
import java.nio.file.Paths;

import javax.swing.JOptionPane;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.autoexam.claimer.controller.ClaimerController;
import com.autoexam.claimer.model.ClaimerModel;
import com.autoexam.claimer.model.enums.Folders;
import com.autoexam.claimer.view.ClaimerView;
import com.autoexam.claimer.view.styles.ClaimerPreset;

/**
 * Entry point of the claimer app.
 * 
 * @author Georgi Iliev
 */
public class ClaimerMain {

    private static final Logger LOGGER = Logger.getLogger(ClaimerMain.class);

    public static void main(String[] args) {
	try {
	    PropertyConfigurator.configure(
		    Paths.get(Folders.HOME_FOLDER.identifier(), Folders.CONFIG_FOLDER.identifier(), "log4j.properties")
			    .toString());

	    ClaimerModel model = new ClaimerModel("claimer.properties");
	    ClaimerView view = new ClaimerView(new ClaimerPreset());
	    ClaimerController controller = new ClaimerController(model, view);

	    controller.initializeData();
	    controller.initializeActions();

	    view.display();

	} catch (IOException ioe) {
	    LOGGER.error("Inaccessible file: ", ioe);

	    JOptionPane.showMessageDialog(null, "Недостъпен файл: " + ioe.getMessage(), "ГРЕШКА",
		    JOptionPane.ERROR_MESSAGE);
	} catch (Exception e) {
	    LOGGER.error("Unexpected error: ", e);

	    JOptionPane.showMessageDialog(null, "Неочаквана грешка(свържи се с програмиста): " + e.getMessage(),
		    "ГРЕШКА", JOptionPane.ERROR_MESSAGE);
	}
    }
}
