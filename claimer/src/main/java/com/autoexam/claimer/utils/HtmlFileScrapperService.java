package com.autoexam.claimer.utils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;

import javax.swing.JOptionPane;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.AbstractWebDriverEventListener;
import org.openqa.selenium.support.events.WebDriverEventListener;

import com.autoexam.claimer.model.enums.Folders;
import com.google.common.io.Files;
import com.sun.jna.platform.win32.Guid.GUID;

/**
 * Temporary file scrapper service - will be removed in later stage.
 * 
 * @author Georgi Iliev
 */
// TODO: [TEMP]
public class HtmlFileScrapperService extends AbstractWebDriverEventListener implements WebDriverEventListener {

    private static final Logger LOGGER = Logger.getLogger(HtmlFileScrapperService.class);

    public static final File TEMP_FOLDER = Paths.get(Folders.HOME_FOLDER.identifier(), Folders.TEMP_FOLDER.identifier())
	    .toFile();

    private static int pageNumber = 0;
    private static int scriptNumber = 0;

    private static String lastUrl;
    private static String lastScript;

    @Override
    public void afterNavigateTo(String url, WebDriver driver) {
	if (url.equals(lastUrl)) {
	    return;
	}
	pageNumber++;

	try {
	    File pageFile = Paths
		    .get(TEMP_FOLDER.toString(), "[Page(" + pageNumber + ")]" + GUID.newGuid().toGuidString() + ".xml")
		    .toFile();

	    Files.createParentDirs(pageFile);
	    Files.write(driver.getPageSource(), pageFile, StandardCharsets.UTF_8);

	    LOGGER.info("Page saved: <<" + pageFile.getPath() + ">>");
	    LOGGER.info("Page(" + driver.getTitle() + ") visited: " + url);

	} catch (IOException ioe) {
	    LOGGER.error("Inaccessible file: ", ioe);

	    JOptionPane.showMessageDialog(null, "Недостъпен файл: " + ioe.getMessage(), "ГРЕШКА",
		    JOptionPane.ERROR_MESSAGE);
	}

	lastUrl = url;

	super.afterNavigateTo(url, driver);
    }

    @Override
    public void afterScript(String script, WebDriver driver) {
	if (script.equals(lastScript)) {
	    return;
	}
	scriptNumber++;

	try {
	    File scriptFile = Paths.get(TEMP_FOLDER.toString(), "[Script(" + scriptNumber + ") for page(" + pageNumber
		    + ")]" + GUID.newGuid().toGuidString() + ".js").toFile();

	    Files.createParentDirs(scriptFile);
	    Files.write(driver.getPageSource(), scriptFile, StandardCharsets.UTF_8);

	    LOGGER.info("Script saved: [" + scriptFile.getPath() + "]");
	    LOGGER.info("Script for page <<" + driver.getTitle() + ">> executed: " + script);

	} catch (IOException ioe) {
	    LOGGER.error("Inaccessible file: ", ioe);

	    JOptionPane.showMessageDialog(null, "Недостъпен файл: " + ioe.getMessage(), "ГРЕШКА",
		    JOptionPane.ERROR_MESSAGE);
	}

	lastScript = script;

	super.afterScript(script, driver);
    }
}
