package com.autoexam.claimer.model.enums;

/**
 * Claimer standard folder names.
 * 
 * @author Georgi Iliev
 */
public enum Folders {
    HOME_FOLDER(System.getProperty("user.dir")), // nl
    TEMP_FOLDER("Stranici"), // nl
    CONFIG_FOLDER("conf"), // nl
    EXE_FOLDER("bin");

    private String folderName;

    Folders(final String folderName) {
	this.folderName = folderName;
    }

    public String identifier() {
	return this.folderName;
    }
}
