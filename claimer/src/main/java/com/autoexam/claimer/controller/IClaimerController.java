package com.autoexam.claimer.controller;

/**
 * Blueprint for an Eloboost controller.
 * 
 * @author Georgi Iliev
 */
public interface IClaimerController extends IRunBrowser {

    /**
     * Initializes the default values of fields in the view(UI).
     */
    void initializeData();

    /**
     * Initializes the actions of the components in the view(UI).
     */
    void initializeActions();
}
