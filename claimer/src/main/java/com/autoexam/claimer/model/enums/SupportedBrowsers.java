package com.autoexam.claimer.model.enums;

import java.nio.file.Paths;

/**
 * Enumeration for supported browsers.
 * 
 * @author Georgi Iliev
 */
public enum SupportedBrowsers {
    MozillaFirefox("webdriver.gecko.driver", "geckodriver.exe", true), // nl
    InternetExplorer("webdriver.ie.driver", "IEDriverServer.exe", true), // nl
    GoogleChrome("webdriver.chrome.driver", "chromedriver.exe", false);

    private String systemProperty;
    private String driverExeName;
    private boolean isRecommended;

    SupportedBrowsers(final String systemProperty, final String driverExeName, final boolean isRecommended) {
	this.systemProperty = systemProperty;
	this.driverExeName = driverExeName;
	this.isRecommended = isRecommended;
    }

    /**
     * Configures the browser for startup by setting the appropriate system
     * property relevant for WebDriver.
     */
    public void configure() {
	System.setProperty(systemProperty,
		Paths.get(Folders.HOME_FOLDER.identifier(), Folders.EXE_FOLDER.identifier(), driverExeName).toString());
    }

    public String getSystemProperty() {
	return this.systemProperty;
    }

    public String getDriverExeName() {
	return this.driverExeName;
    }

    public boolean isSelectable() {
	return this.isRecommended;
    }
}
