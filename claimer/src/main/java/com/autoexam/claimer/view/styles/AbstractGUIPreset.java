package com.autoexam.claimer.view.styles;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

/**
 * Abstract representation of a GUI preset.
 * 
 * @author Georgi Iliev
 */
public abstract class AbstractGUIPreset implements IGUIPreset {

    protected Color frameBackground;
    protected Color valueLabelForeground;
    protected Color valueComponentForeground;
    protected Color valueComponentBackground;
    protected Color valueComponentSelectionForeground;
    protected Color valueComponentSelectionBackground;

    protected Font buttonFont;
    protected Font valueLabelFont;
    protected Font valueComponentFont;

    protected int buttonHeight;
    protected int buttonWidth;

    protected int valueLabelWidth;
    protected int valueLabelHeight;

    protected int valueComponentWidth;
    protected int valueComponentHeight;

    public AbstractGUIPreset() {
	frameBackground = Color.GREEN;

	valueComponentForeground = Color.BLACK;
	valueComponentBackground = Color.WHITE;
	valueComponentSelectionForeground = Color.WHITE;
	valueComponentSelectionBackground = Color.BLACK;

	valueLabelForeground = Color.BLACK;
    }

    @Override
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public void style(final Component component) {
	if (component instanceof JTextField) {
	    styleTextField((JTextField) component);
	} else if (component instanceof JComboBox) {
	    styleComboBox((JComboBox) component);
	} else if (component instanceof JLabel) {
	    styleLabel((JLabel) component);
	} else if (component instanceof JButton) {
	    styleButton((JButton) component);
	} else if (component instanceof JScrollPane) {
	    styleScrollPane((JScrollPane) component);
	} else if (component instanceof JFrame) {
	    styleFrame((JFrame) component);
	}
    }

    public abstract void styleFrame(final JFrame frame);

    protected abstract void styleValueDisplayComponent(final JComponent component);

    public abstract void styleLabel(final JLabel label);

    public abstract void styleTextField(final JTextField field);

    public abstract void styleButton(final JButton button);

    public abstract <E> void styleComboBox(final JComboBox<E> comboBox);

    public abstract void styleScrollPane(final JScrollPane scroll);

}
