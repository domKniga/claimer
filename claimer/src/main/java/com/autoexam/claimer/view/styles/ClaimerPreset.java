package com.autoexam.claimer.view.styles;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import com.autoexam.claimer.view.customizers.CustomComboBoxRenderer;

/**
 * The concrete preset implementation for displays with HD resolution.
 * 
 * @author Georgi Iliev
 */
public class ClaimerPreset extends AbstractGUIPreset {

    private static final int COMPONENT_FONTSIZE = 14;
    private static final int BUTTON_FONTSIZE = 16;
    private static final String FONT_FAMILY = "Tahoma";

    public ClaimerPreset() {
	buttonHeight = 100;
	buttonWidth = 520;
	buttonFont = new Font(FONT_FAMILY, Font.BOLD, BUTTON_FONTSIZE);

	valueLabelFont = new Font(FONT_FAMILY, Font.BOLD, COMPONENT_FONTSIZE);
	valueComponentFont = new Font(FONT_FAMILY, Font.BOLD + Font.ITALIC, COMPONENT_FONTSIZE);

	valueLabelWidth = 200;
	valueLabelHeight = 30;

	valueComponentWidth = 300;
	valueComponentHeight = 30;
    }

    @Override
    protected void styleValueDisplayComponent(final JComponent component) {
	component.setPreferredSize(new Dimension(valueComponentWidth, valueComponentHeight));
	component.setFont(valueComponentFont);
    }

    @Override
    public void styleTextField(final JTextField field) {
	styleValueDisplayComponent(field);
	field.setForeground(valueComponentForeground);
	field.setBackground(valueComponentBackground);
	field.setSelectedTextColor(valueComponentSelectionForeground);
	field.setSelectionColor(valueComponentSelectionBackground);
    }

    @Override
    public <E> void styleComboBox(final JComboBox<E> comboBox) {
	styleValueDisplayComponent(comboBox);

	comboBox.setRenderer(new CustomComboBoxRenderer(valueComponentFont, valueComponentForeground,
		valueComponentBackground, valueComponentSelectionForeground, valueComponentSelectionBackground));
    }

    @Override
    public void styleLabel(final JLabel label) {
	label.setOpaque(false);
	label.setPreferredSize(new Dimension(valueLabelWidth, valueLabelHeight));
	label.setForeground(valueLabelForeground);
	label.setFont(valueLabelFont);
    }

    @Override
    public void styleButton(final JButton button) {
	button.setOpaque(false);
	button.setFont(buttonFont);
	button.setPreferredSize(new Dimension(buttonWidth, buttonHeight));
    }

    @Override
    public void styleScrollPane(final JScrollPane scroll) {
	scroll.setOpaque(false);
	scroll.getViewport().setOpaque(false);
	scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
	scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    }

    @Override
    public void styleFrame(final JFrame frame) {
	frame.setResizable(false);
	frame.getContentPane().setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
	frame.getContentPane().setBackground(Color.GREEN);
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
