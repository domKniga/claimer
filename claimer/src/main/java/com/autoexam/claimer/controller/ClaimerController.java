package com.autoexam.claimer.controller;

import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.JOptionPane;

import org.apache.log4j.Logger;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.autoexam.claimer.model.ClaimerModel;
import com.autoexam.claimer.model.enums.SupportedBrowsers;
import com.autoexam.claimer.utils.HtmlFileScrapperService;
import com.autoexam.claimer.view.ClaimerView;
import com.autoexam.claimer.view.customizers.BrowserPickerModel;

/**
 * The claimer controller(driver/logic).
 * 
 * @author Georgi Iliev
 */
public class ClaimerController implements IClaimerController {

    private static final Logger LOGGER = Logger.getLogger(ClaimerController.class);

    private ClaimerModel model;
    private ClaimerView view;

    private EventFiringWebDriver eventFiringBrowser;

    /**
     * @param model
     *            - the claimer model.
     * @param view
     *            - the claimer view(UI).
     */
    public ClaimerController(final ClaimerModel model, final ClaimerView view) {
	this.model = model;
	this.view = view;
    }

    @Override
    public void initializeData() {
	view.getBrowserPicker().setModel(new BrowserPickerModel(model.getBrowsers()));
	view.getURLField().setText(model.getStartingUrl());
	view.getURLField().setToolTipText(model.getStartingUrl());
	view.getLicenseField().setText(model.getLicenseNumber());
    }

    @Override
    public void initializeActions() {
	view.getStartButton().addActionListener(startBrowsing -> runBrowser(startBrowsing));
	view.addWindowListener(new WindowAdapter() {
	    @Override
	    public void windowClosing(WindowEvent windowEvent) {
		try {
		    if (eventFiringBrowser != null) {
			LOGGER.info("Closing browser instances/processes...");
			closeBrowserInstance();
		    }

		    int saveSettings = JOptionPane.showConfirmDialog(view, "Запаметяване на въведените стойности?",
			    "ИЗБОР", JOptionPane.YES_NO_OPTION);

		    if (saveSettings == JOptionPane.YES_OPTION) {
			LOGGER.info("Saving current settings...");
			saveSettings();
		    }

		} catch (IOException ioe) {
		    LOGGER.error("Inaccessible file: ", ioe);

		    JOptionPane.showMessageDialog(view, "Недостъпен файл: " + ioe.getMessage(), "ГРЕШКА",
			    JOptionPane.ERROR_MESSAGE);
		} finally {
		    LOGGER.info("Finishing up & closing window...");
		    LOGGER.info("----------------------------");
		}

		super.windowClosing(windowEvent);
	    }
	});
    }

    @Override
    public void runBrowser(ActionEvent startBrowsing) {
	RemoteWebDriver browserInstance = null;

	final SupportedBrowsers selectedBrowser = (SupportedBrowsers) view.getBrowserPicker().getSelectedItem();
	LOGGER.info("Selected Browser: " + selectedBrowser.toString());

	selectedBrowser.configure();

	try {
	    switch (selectedBrowser) {
	    case GoogleChrome: // CURRENTLY NOT ALLOWED
		browserInstance = new ChromeDriver();
		break;
	    case MozillaFirefox:

		browserInstance = new FirefoxDriver();

		break;
	    case InternetExplorer:
	    default:
		browserInstance = new InternetExplorerDriver();
		break;
	    }

	    eventFiringBrowser = new EventFiringWebDriver(browserInstance);
	    eventFiringBrowser.register(new HtmlFileScrapperService());
	    eventFiringBrowser.manage().window().maximize();
	    eventFiringBrowser.get(view.getURLField().getText());
	} catch (RuntimeException re) {
	    LOGGER.error(re.getMessage());

	    JOptionPane.showMessageDialog(view, "Връзката с браузъра на може да се осъществи!", "ГРЕШКА",
		    JOptionPane.ERROR_MESSAGE);
	}
    }

    private void closeBrowserInstance() throws IOException {
	String[] cleanupCommands = null;

	if (!(eventFiringBrowser.getWrappedDriver() instanceof FirefoxDriver)) {
	    eventFiringBrowser.quit();
	    cleanupCommands = new String[] { "taskkill /im IEDriverServer.exe /f" };
	} else {
	    cleanupCommands = new String[] { // nl
		    "taskkill /im geckodriver.exe /f", // nl
		    "taskkill /im firefox.exe /f" // nl
	    };
	}

	for (String cmd : cleanupCommands) {
	    Runtime.getRuntime().exec(cmd);
	}
    }

    private void saveSettings() throws IOException {
	Map<String, String> currentSettings = new LinkedHashMap<>();
	currentSettings.put(ClaimerModel.KEY_STARTING_URL, view.getURLField().getText());
	currentSettings.put(ClaimerModel.KEY_LICENSE_NUMBER, view.getLicenseField().getText());

	if ("".equals(currentSettings.get(ClaimerModel.KEY_STARTING_URL))) {
	    currentSettings.put(ClaimerModel.KEY_STARTING_URL, ClaimerModel.DEFAULT_STARTING_URL);
	}

	if ("".equals(currentSettings.get(ClaimerModel.KEY_LICENSE_NUMBER))) {
	    currentSettings.put(ClaimerModel.KEY_LICENSE_NUMBER, ClaimerModel.DEFAULT_LICENSE_NUMBER);
	}

	model.saveSettings(currentSettings);
    }
}
